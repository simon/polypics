CFLAGS = -g

IMAGES = icos-bw-vert icos-colour-vert icos-bw-face icos-colour-face cube-colour-face cube-colour-vert
PSPRE = $(patsubst %,%-pre.ps,$(IMAGES))
PDF = $(patsubst %,%.pdf,$(IMAGES))
NETSPEC =

all: $(PDF)

$(PSPRE): %.ps:
	$$HOME/src/polyhedra/drawnet.py $(NETSPEC) \
		-p$^ | \
	sed > $@ \
		-e 's/288 400 translate/288 430 translate/' \
		-e 's/[0-9\.]* dup scale/70 dup scale/' \
		-e 's/[0-9\.]* setlinewidth/0.002 setlinewidth/'

$(filter icos-%, $(PSPRE)): override NETSPEC = -sEGB -aIFC,IKF -aFHC,IFC -aDHF,FHC
cube-colour-face-pre.ps: override NETSPEC = -aface2,face5
cube-colour-vert-pre.ps: override NETSPEC = -R 45 -sface5 -aface2,face5 -aface5,face3

icos-bw-vert-pre.ps: mapbw.ps icos-vertices
icos-colour-vert-pre.ps: mapcolour.ps icos-vertices
icos-bw-face-pre.ps: mapbw.ps icos-faces
icos-colour-face-pre.ps: mapcolour.ps icos-faces
cube-colour-vert-pre.ps: mapcolour.ps cube-vertices
cube-colour-face-pre.ps: mapcolour.ps cube-faces

$(PDF): %.pdf:
	gs -dNOSAFER -sDEVICE=pdfwrite -sOutputFile=$@ -sPAPERSIZE=a4 -dBATCH -dNOPAUSE -q $<

icos-bw-vert.pdf: icos-bw-vert-pre.ps desc.bw
icos-colour-vert.pdf: icos-colour-vert-pre.ps desc.colour
icos-bw-face.pdf: icos-bw-face-pre.ps desc.bw
icos-colour-face.pdf: icos-colour-face-pre.ps desc.colour

cube-colour-vert.pdf: cube-colour-vert-pre.ps desc.colour
cube-colour-face.pdf: cube-colour-face-pre.ps desc.colour

# Transformation matrix derived in Maxima using this command:
# float(subst([c=sqrt(1/2),s=sqrt(1/2),C=sqrt(1/3),S=-sqrt(2/3)], matrix([C,0,-S],[0,1,0],[S,0,C]) . matrix([c,-s,0],[s,c,0],[0,0,1])));
# which first rotates by pi/4 about the z-axis (pole to pole) to bring
# a cube edge to the Greenwich meridian, and then rotates about the
# y-axis (perpendicular to the plane of the meridian) by the right
# angle to bring a vertex to the north pole.
cube-vertices: cube-faces
	$$HOME/src/polyhedra/transform.py 0.408248290463863 0.7071067811865475 -0.5773502691896258 -0.408248290463863 0.7071067811865475 0.5773502691896258 0.8164965809277262 0.0 0.5773502691896258 $< $@

desc.bw: makebmp makemap.sh
	./makemap.sh

desc.colour: makebmp makecmap.sh
	./makecmap.sh

makebmp: makebmp.c
	gcc -o makebmp makebmp.c

CUBEFACES = 1 2 3 4 5 6
CUBEFACEPNGS = $(patsubst %,cube%.png,$(CUBEFACES))
CUBEFACEBMPS = $(patsubst %,cube%.bmp,$(CUBEFACES))
ANGLES = 000 015 030 045 060 075 090 105 120 135 150 165 \
         180 195 210 225 240 255 270 285 300 315 330 345
CUBEPICS = $(patsubst %,cubevert%.png,$(ANGLES))
cube: $(CUBEPICS)
$(CUBEPICS): cubevert%.png: $(CUBEFACEPNGS) cube.pov
	povray +fn +Icube.pov +O$@ +W500 +H500 +V -D +X +K$(patsubst cubevert%.png,%,$@)
$(CUBEFACEPNGS): %.png: %.bmp
	convert $< $@
$(CUBEFACEBMPS): makecube
	./makecube -v 500

makecube: makecube.c
	gcc -o makecube makecube.c -lm

icon: bwglobesmall.jpeg bwglobelarge.jpeg \
      cglobesmall.jpeg cglobelarge.jpeg

bwglobesmall.jpeg: bwglobe.png
	convert -scale 150x150 $< $@
bwglobelarge.jpeg: bwglobe.png
	convert -scale 450x450 $< $@
cglobesmall.jpeg: cglobe.png
	convert -scale 150x150 $< $@
cglobelarge.jpeg: cglobe.png
	convert -scale 450x450 $< $@

cglobe.png: icosc.pov icos-colour-vert-frag.pov
	povray +fn +I$< +O$@ +W900 +H900 +V -D +X

icos-colour-vert-frag.pov: icos-vertices mapcolour.ps desc.colour
	$$HOME/src/polyhedra/povpoly.py -pmapcolour.ps icos-vertices $@

bwglobe.png: icosb.pov icos-bw-vert-frag.pov
	povray +fn +I$< +O$@ +W900 +H900 +V -D +X

icos-bw-vert-frag.pov: icos-vertices mapbw.ps desc.bw
	$$HOME/src/polyhedra/povpoly.py -pmapbw.ps icos-vertices $@

clean:
	rm -f makebmp makecube *.eps *.bmp *.shape desc.* *-pre.ps Memory.Log
	rm -f cube?.bmp cube?.png bwglobe.png cglobe.png
	rm -f *-frag.pov* Memory.Log

spotless: clean
	rm -f *.pdf cubevert*.png *globe.png *.jpeg
